require 'active_record'
require 'fileutils'

ActiveRecord::Base.logger = Logger.new(STDERR)

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: 'db/test.sqlite3'
)


# models

class Tag < ActiveRecord::Base
  belongs_to :video
end

class Video < ActiveRecord::Base
  has_many :tags

  def add_tags(tag_names)
    tag_names.each do |name|
      Tag.create(name: name, video_id: id)
    end
  end
end
