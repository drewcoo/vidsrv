require 'sinatra'
require 'sinatra/activerecord'
require File.expand_path('database', __dir__)

require File.expand_path('routes/api', __dir__)
require File.expand_path('routes/ui', __dir__)

class MyApp < Sinatra::Application
  # set :database, {adapter: "sqlite3", load_config: "config/database.yml"}
  set :database_file, '../config/database.yml'

  run! if app_file == $PROGRAM_NAME
end
