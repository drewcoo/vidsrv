require 'faraday'
require 'json'

def omdb_url(key_path = '../local_data/omdb_api_key.txt')
  key_file = File.expand_path(key_path, __dir__)
  key = File.read(key_file).chomp
  "http://www.omdbapi.com/?apikey=#{key}"
end

OMDB_BASE_URL = omdb_url.freeze

class MovieOracle
  def self.escape(string)
    string.tr(' ', '+').downcase
  end

  def self.title(unescaped_title)
    response = Faraday.get("#{OMDB_BASE_URL}&t=#{escape(unescaped_title)}")
    JSON.parse(response.body)
  end

  def self.imdb_id(id)
    response = Faraday.get("#{OMDB_BASE_URL}&i=#{id}")
    JSON.parse(response.body)
  end
end
=begin
#
# Get imdb ids from the file,
# query omdb (only, for now),
# and store all moves as json file.
#
id_data = File.read('local_data/imdb_ids.txt')

movie_data = []
id_data.each_line do |id|
  movie_data << MovieOracle.imdb_id(id.strip)
end

File.open('local_data/omdbapi_data.json', 'w') do |f|
  f.write(movie_data.to_json)
end

#
# Print out the file so I can see that it worked.
#
under = JSON.parse(File.read('local_data/omdbapi_data.json'))
pp under
=end
