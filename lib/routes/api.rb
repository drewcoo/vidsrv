require 'sinatra/json'
require File.expand_path('api/v0.1', __dir__)

class MyApp < Sinatra::Application
  get '/api' do
    json(versions: ['v0.1'])
  end
end
