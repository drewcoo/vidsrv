require 'faraday'

class MyApp < Sinatra::Application
  def api_get(url)
    api = 'http://localhost:4567/api/v0.1'
    puts "GET: #{api}/#{url}"
    JSON.parse(Faraday.get("#{api}/#{url}").body)
  end

  get '/' do
    erb :index
  end

  get '/videos' do
    data = api_get('videos')
    erb :video_list, locals: { videos: data }
  end

  get '/videos/:title' do
    videos = api_get("videos?title=#{params['title']}") # .first
    puts "\n\nVIDEOS? #{videos}"

    videos.each do |v|
      puts "V_ID: #{v['id']}"
      tags = api_get("tags?video_id=#{v['id']}")
      v['tags'] = tags
    end

    erb :multiple_videos, locals: { videos: videos }

    # erb :single_video, locals: { video: videos[0], tags: videos[0]['tags'] }
    # tags = api_get("tags?video_id=#{video_id_str}")
    # erb :single_video, locals: { video: data }
    # erb :single_video, locals: { video: Video.find_by_title(params['title']) }
  end

  get '/tags' do
    data = api_get('tags')
    puts "\n\nDATA: #{data}\n\n"
    erb :tag_list, locals: { tags: data }
  end

  get '/tags/:name' do
    tagname = params['name'].gsub('%20', ' ')

    tags = api_get("tags?name=#{tagname}")
    video_id_str = tags.map { |t| t['video_id'] }.join(',')

    videos = api_get("videos?id=#{video_id_str}")

    video_tags = api_get("tags?video_id=#{video_id_str}")
    video_tags.delete_if { |t| t['name'] == tagname }

    erb :single_tag, locals: { tagname: tagname,
                               videos: videos,
                               video_tags: video_tags }
  end
end
