require_relative('api_helper')

class MyApp < Sinatra::Application
  @prefix = '/api/v0.1'

  get @prefix.to_s do
    json(one: 1, two: 'too')
  end

  get '/params' do
    "#{params}<br>\n" \
      "#{params.size}"
  end

  get "#{@prefix}/tags" do
    json(tags_by_params(params))
  end

  get "#{@prefix}/tags/:id" do
    json(Tag.find_by_id(params[:id]))
  end

  get "#{@prefix}/videos" do
    json(videos_by_params(params))
  end

  get "#{@prefix}/videos/:id" do
    json(Video.find_by_id(params[:id]))
  end
end
