def tags_by_name(name)
  # if title starts with '~', look for all titles matching substr
  if name[0] == '~'
    name.delete_prefix!('~')
    Tag.where('name like ?', "%#{name}%").all
  else
    # titles can have commas; lots of possible parse problems;
    # just treat as one title
    Tag.find_by_name(name)
  end
end

def tags_by_params(params)
  if params[:id]
    Tag.find_by_id(params[:id])
  elsif params[:name]
    Tag.where(name: params[:name])
  elsif params[:video_id]
    Tag.where(video_id: params[:video_id])
  else
    Tag.all
  end
end

def videos_by_id(id)
  # Why is this like this?
  # If it has to be a stringified array, does something like this make sense?
  # id = JSON.parse(id)
  #
  id = id.sub('[', '').sub(']', '').split(',').map(&:to_i)
  Video.find(id)
end

def videos_by_params(params)
  if params[:title]
    videos_by_title(params[:title])
  elsif params[:id]
    videos_by_id(params[:id])
  else
    Video.all
  end
end

def videos_by_title(title)
  # if title starts with '~', look for all titles matching substr
  if title[0] == '~'
    title.delete_prefix!('~')
    Video.where('title like ?', "%#{title}%").all
  else
    # titles can have commas; lots of possible parse problems;
    # just treat as one title
    Video.where(title: title)
  end
end
