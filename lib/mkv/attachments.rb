require 'json'
require 'tmpdir'

module MKV
  class Attachments
    def initialize(name)
      @name = name
    end

    def add(file)
      raise unless system "#{MKV::PROP_EDIT} #{@name} --add-attachment #{file} >nul"
    end

    def delete_at(index)
      raise "arg must be integer: \"#{index}\"" unless index.instance_of?(Integer)
      raise unless system "#{MKV::PROP_EDIT} #{@name} --delete-attachment #{index} >nul"
      list
    end

    def delete_all(target_name)
      kill_list = []
      list.each { |index, name| kill_list << index if name == target_name }
      # reverse to avoid changing the indexes we're trying to delete
      kill_list.reverse_each { |index| delete_at(index) }
      list
    end

    def list
      # backtickles - hehe!
      out = `#{MKV::MERGE} -i #{@name}`
      result = {}
      out.scan(/ID (\d+): .+ name \'([^\']*)\'/) { |a, b| result[a.to_i] = b }
      result
    end

    # Extract the attachment at the index to the file given.
    def extract(index, file)
      raise unless system "#{MKV::EXTRACT} attachments #{@name} #{index}:#{file} >nul"
    end

    # read attachment by name
    # assume there is only one by that name
    # return the data, discard file created along the way
    def read(name: MKV::DEFAULT_ATTACHMENT)
      index = list.key(name)
      raise "attachment not found: #{name}" unless index

      tmpdir = Dir.mktmpdir
      filename = ::File.join(tmpdir, name)
      extract(index, filename)
      result = ::File.read(filename).strip
      FileUtils.rm_rf(tmpdir)
      result
    end

    # WARNING: This clobbers all same-named attachments by default.
    def write(string, name: MKV::DEFAULT_ATTACHMENT, overwrite: true)
      # fail "ATTACH NAME: \"#{name}\""
      delete_all(name) if overwrite

      tmpdir = Dir.mktmpdir
      filename = ::File.join(tmpdir, name)
      ::File.open(filename, 'w') { |f| f.write(string); f.flush }
      # fail "FILE: #{filename}"
      add(filename)
      FileUtils.rm_rf(tmpdir)
    end
  end
end
