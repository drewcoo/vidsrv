module MKV
  class File
    attr_accessor :attachments

    def initialize(name)
      @name = name
      @attachments = Attachments.new(name)
    end

    def title
      out = `#{MKV::INFO} #{@name}  |findstr Title`
      # From the _first_ mathing line return the _first_ match
      out.scan(/Title: (.*)/).first.first
    rescue NoMethodError
      ''
    end

    def title=(new_title)
      system "#{MKV::PROP_EDIT} #{@name} -e info -s title=\"#{new_title}\" >nul"
    end
  end
end
