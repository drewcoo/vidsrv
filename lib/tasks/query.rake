require_relative '../database'

namespace :query do
  task :title, [:param1] do |_task, args|
    p Video.where(title: args[:param1])
  end

  task tag: ['tag:default']

  desc 'call with tag="FOO"'
  namespace :tag do
    desc 'all tags'
    task :all do
      pp @all_tags = Tag.all
    end

    desc 'all - just the names'
    task :allnames do
      Rake::Task['query:tag:all'].invoke
      pp @all_tags.group(:name).map(&:name)
    end

    task :default do
      pp @tags = Tag.where(name: ENV['tag'].split(','))
    end

    desc 'tagged videos'
    task :videos do
      Rake::Task['query:tag'].invoke
      video_ids = @tags.map(&:video_id)
      @videos = video_ids.map { |i| Video.find_by_id(i) }
      pp @videos.map { |v| [v, Tag.where(video_id: v.id)] }
    end
  end
end
