require File.expand_path('../mkv', __dir__)

namespace :mkv do
  namespace :title do
    desc 'mkv:title:get file=FOO'
    task :get do
      puts MKV::File.new(ENV['file']).title
    end

    desc 'mkv:title:set file=FOO title=BAR'
    task :set do
      MKV::File.new(ENV['file']).title = ENV['title']
    end
  end

  namespace :attachments do
    desc 'mkv:attachments:add file=FOO '
    task :add do
      MKV::File.new(ENV['file']).attachments.add(ENV['file'])
    end

    desc 'mkv:attachments:delete_all file=FOO name=BAR'
    task :delete_all do
      MKV::File.new(ENV['file']).attachments.delete_all(ENV['name'])
    end

    desc 'mkv:attachments:delete_at file=FOO index=BAR'
    task :delete_at do
      MKV::File.new(ENV['file']).attachments.delete_at(ENV['index'].to_i)
    end

    desc 'mkv:attachments:extract file=FOO index=BAR out=BAZ'
    task :extract do
      puts MKV::File.new(ENV['file']).attachments.extract(ENV['index'].to_i,
                                                          ENV['out'])
    end

    desc 'mkv:attachments:list file=FOO'
    task :list do
      puts MKV::File.new(ENV['file']).attachments.list
    end

    desc 'mkv:attachments:read file=FOO [name=BAR]'
    task :read do
      # TODO: handle name
      # defaults to name: DEFAULT_ATTACHMENT
      puts MKV::File.new(ENV['file']).attachments.read(name: ENV['name'])
    end

    desc 'mkv:attachments:write file=FOO data=BAR [name=BAZ] [overwrite=true]'
    task :write do
      # TODO: handle name and overwrite
      MKV::File.new(ENV['file']).attachments.write(ENV['data'],
                                                   name: ENV['name'])
      #    def write_json(string, name: DEFAULT_ATTACHMENT, overwrite: true)
    end
  end
end
