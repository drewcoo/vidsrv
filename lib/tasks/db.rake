require 'sinatra/activerecord/rake'

namespace :db do
  task :load_config do
    require './lib/app'
  end

  desc 'Import the data from data/initial_videos.txt'
  task :ingest do
    require_relative '../../db/ingest/ingest_store'
    ingester = IngestStore.new
    ingester.ingest
  end

  task :injest do
    puts <<-JOKE

  A SQL query walks into a bar, goes up to a table, and asks "May I join you?"
    JOKE
  end
end
