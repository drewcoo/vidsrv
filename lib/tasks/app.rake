require 'faraday'

namespace :app do
  @app_name = 'video_server'

  def running?
    Faraday.get('http://localhost:4567/')
    true
  rescue Faraday::ConnectionFailed
    false
  end

  desc 'start the app'
  task :start do
    raise 'ERROR: already running' if running?

    app_path = File.expand_path('../app.rb', __dir__)
    command = "start \"#{@app_name}\" \"#{app_path}\""
    # command = "ruby #{__FILE__} server #{@port}"
    # command = if ENV['OS'] == 'Windows_NT'
    #            "start \"doppelserver\" cmd /c #{command}"
    #          else
    #            "nohup #{command} &"
    #          end
    system command
    sleep 0.1 until running?
  end

  task :stop do
    command = "taskkill /F /FI \"WINDOWTITLE eq #{@app_name}\""
    system command
  end

  task :restart do
    Rake::Task['app:stop'].invoke
    Rake::Task['app:start'].invoke
  end
end
