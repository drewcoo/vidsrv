require_relative '../vlc'

namespace :vlc do
  desc 'play file=<MKV file>'
  task :play do
    VLC.play(ENV['file'])
  end

  desc 'stop'
  task :stop do
    VLC.stop
  end

  desc 'pause'
  task :pause do
    VLC.pause
  end

  desc 'Is it running?'
  task 'running?' do
    VLC.running?
  end
end
