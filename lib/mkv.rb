#
# requires MKVToolNix to work
# can 'choco install y MKVToolNix'
#

# Require everything in the mkv dir.
mkv_dir = File.expand_path('mkv', __dir__)
Dir.glob(File.join(mkv_dir, '*.rb')).each { |f| require f }

module MKV
  DEFAULT_ATTACHMENT = 'omdbapi.json'.freeze
  TOOL_NIX_DIR = '%ProgramFiles%\\MKVToolnix\\'.freeze
  EXTRACT = "\"#{TOOL_NIX_DIR}mkvextract.exe\"".freeze
  INFO = "\"#{TOOL_NIX_DIR}mkvinfo.exe\"".freeze
  MERGE = "\"#{TOOL_NIX_DIR}mkvmerge.exe\"".freeze
  PROP_EDIT = "\"#{TOOL_NIX_DIR}mkvpropedit.exe\"".freeze
end
