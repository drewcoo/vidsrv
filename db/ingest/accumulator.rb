class Accumulator
  attr_reader :state

  def initialize
    @state = []
  end

  def <<(item)
    return clear if item.nil? || item.empty?

    @state << item
    nil
  end

  def clear
    return if @state.empty?

    result = @state
    @state = []
    result
  end
end
