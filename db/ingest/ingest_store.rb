require_relative '../../lib/database'
require_relative 'movie_struct'
require_relative 'accumulator'

class IngestStore
  def initialize
    @initial_data = File.expand_path('../../local_data/initial_videos.txt',
                                     __dir__)
  end

  # NSV == Newline Separated Values
  #
  # Read the input into an array of stanzas.
  # Stanzas are arrays of unseparated lines from the file.
  # For each group of multiple newlines we separate the outer array.
  #
  # "1\n2\n\n3\n\n\n\n4\n" --> [['1','2'], ['3'], ['4']]
  #
  def __read_NSV(filename)
    result = []
    entry_accum = Accumulator.new
    File.read(filename).split("\n").each do |line|
      popped = entry_accum << line.strip
      result << popped unless popped.nil?
    end
    result << (entry_accum << '')
  end

  # Take a stanze as read by __read_NSV and turn it onto a Movie object.
  # A movie is just a PORO that also knows how to write its data to the db.
  #
  def __stanza_to_movie(stanza)
    tags = (stanza[4] || '').split(',').each(&:strip!)
    Movie.new(title: stanza[0],
              path: stanza[1],
              imdb: stanza[2],
              rotten_tomatoes: stanza[3],
              tags: tags)
  rescue NoMethodError => ex
    puts "\nData file must have stanzas of 4-5 lines"
    puts "of text separated by blank lines.\n\n"
    raise ex
  end

  # Pass the array of stanzas from __read_NSV through the __stanza_to_movie
  # method to give us an array of Movie objects.
  #
  def read_from_flat_file(filename = @initial_data)
    # Read the file into
    __read_NSV(filename).map { |fields| __stanza_to_movie(fields) }
  end

  # Reads a flat file and inserts Movie data into the db.
  #
  def ingest
    read_from_flat_file.each(&:add_to_db)
  end
end
