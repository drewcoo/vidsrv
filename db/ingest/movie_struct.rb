require_relative '../../lib/database'
require 'ostruct'

class Movie < OpenStruct
  def add_to_db
    vid = Video.create(self.to_h.reject { |key| key == :tags })
    vid.add_tags(self.tags)
  end
end
