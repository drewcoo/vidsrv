class InitialTables < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |table|
      table.string :title, null: false
      table.string :path, null: false
      table.string :imdb
      table.string :rotten_tomatoes
    end

    create_table :tags do |table|
      table.string :name
      table.integer :video_id
    end
  end
end
