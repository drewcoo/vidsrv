ENV['APP_ENV'] = 'test'

require 'spec_helper'
require 'app'
require 'rack/test'

RSpec.describe MyApp do
  include Rack::Test::Methods

  def app
    MyApp
  end

  it 'returns api version' do
    get '/api'
    expect(last_response).to be_ok
    expect(JSON.parse(last_response.body)['versions']).to eq(['v0.1'])
  end

  it 'fails on bad api version' do
    get '/api/v0.2'
    expect(last_response).not_to be_ok
  end

#  it "says hello" do
#    get '/'
#    expect(last_response).to be_ok
#    expect(last_response.body).to eq('Hello World')
#  end
end
