require 'spec_helper'
require 'mkv'
require 'tmpdir'

RSpec.describe MKV::File do
  let!(:temp_dir) do
    @tmp_dir_cleanup_var = Dir.mktmpdir
  end

  let!(:mkv) do
    new_file = File.join(temp_dir, 'test.mkv')
    FileUtils.cp(MKV_PATH, new_file)
    MKV::File.new(new_file)
  end

  after(:each) do |it|
    if it.exception
      puts "Kept files: #{@tmp_dir_cleanup_var}"
    else
      FileUtils.rm_rf(@tmp_dir_cleanup_var) if @tmp_dir_cleanup_var
    end
  end

  context '.title' do
    it 'is empty by default' do
      expect(mkv.title).to be_empty
    end

    it 'can be written (and read)' do
      mkv.title = 'foo'
      expect(mkv.title).to eq('foo')
    end
  end

  context '.attachments' do
    context '.list' do
      it 'is empty by default' do
        expect(mkv.attachments.list).to be_empty
      end

      it 'reflects added elements' do
        num = rand(1..5)
        num.times { mkv.attachments.add(JSON_PATH) }
        expect(mkv.attachments.list.length).to eq(num)
      end

      it 'has integer-keyed pairs' do
        mkv.attachments.add(JSON_PATH)
        expect(mkv.attachments.list.keys.first).to be_instance_of(Integer)
      end
    end

    context '.add' do
      it 'can add an attachment' do
        mkv.attachments.add(JSON_PATH)
        expect(mkv.attachments.list.size).to eq(1)
        expect(mkv.attachments.list[1]).to eq(File.split(JSON_PATH).last)
      end

      it 'raises when invalid argument' do
        expect { mkv.attachments.add('I_DO_NOT_EXIST') }.to raise_error(RuntimeError)
      end
    end

    context '.delete_at' do
      it 'can delete an attachment at one-based index' do
        mkv.attachments.add(JSON_PATH)
        expect(mkv.attachments.delete_at(1)).to be_empty
      end

      it 'raises if not passed integer' do
        expect { mkv.attachments.delete_at('foo') }.to raise_error(RuntimeError)
      end

      it 'raises if passed invalid integer' do
        expect { mkv.attachments.delete_at(rand(5)) }.to raise_error(RuntimeError)
      end
    end

    context '.delete_all' do
      it 'does not blow up on empty lists' do
        expect(mkv.attachments.delete_all('foo')).to be_empty
      end

      it 'deletes multiple attachments of only that name' do
        mkv.attachments.add(JSON_PATH)
        mkv.attachments.add(JPG_PATH)
        mkv.attachments.add(JSON_PATH)
        mkv.attachments.delete_all(File.split(JPG_PATH).last)
        expect(mkv.attachments.list.size).to eq(2)
      end
    end

    context '.extract' do
      it 'raises when extracting nothing' do
      end

      it 'extracts data' do
        mkv.attachments.add(JSON_PATH)
        file_name = File.join(temp_dir, 'extracted.json')
        mkv.attachments.extract(1, file_name)
        expect(FileUtils.compare_file(JSON_PATH, file_name)).to be true
      end
    end

    context '.read' do
      it 'can read the default attachment' do
        old_data = JSON.parse(File.read(JSON_PATH))
        mkv.attachments.add(JSON_PATH)
        new_data = JSON.parse(mkv.attachments.read)
        expect(old_data).to eq(new_data)
      end
    end

    context '.write_json' do
      it 'can write to default attachment' do
        # Swapping to json and back so that we're comparing json->hash
        # in both cases. It's less thinking than symbol/string substitution.
        initial_data_json = { foo: 1, bar: [1, 2, 3, 'what?'] }.to_json
        mkv.attachments.write(initial_data_json)
        initial_data_string = JSON.parse(initial_data_json)
        final_data_string = JSON.parse(mkv.attachments.read)
        expect(initial_data_string).to eq(final_data_string)
      end
    end
  end
end
