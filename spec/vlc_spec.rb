require 'spec_helper'
require 'vlc'

RSpec.describe VLC do
  after(:each) do
    VLC.stop if VLC.running?
  end

  context '.play' do
    before(:each) do
      VLC.play(MKV_PATH)
    end

    it '.running?' do
      expect(VLC.running?).to be true
    end

    xit '.pause' do
      # How do I know I'm paused?
    end

    it 'cannot play another' do
      expect { VLC.play(MKV_PATH) }.to raise_error(RuntimeError)
    end
  end
end
