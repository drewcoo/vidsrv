require 'simplecov'
require 'coveralls'

SimpleCov.formatters = \
  SimpleCov::Formatter::MultiFormatter.new(
    [SimpleCov::Formatter::HTMLFormatter,
     Coveralls::SimpleCov::Formatter]
  )

SimpleCov.start do
  add_filter 'app/secrets'
end


MKV_PATH  = File.expand_path('data/EdrCalibration.mkv', __dir__)
JPG_PATH  = File.expand_path('data/cover.jpg', __dir__)
JSON_PATH = File.expand_path('data/omdbapi.json', __dir__)

ENV['APP_ENV'] = 'test'

require 'rspec'

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

RSpec.configure do |config|
  config.color = true
  config.failure_color = :cyan
  config.disable_monkey_patching!
  config.fail_fast = 0
  config.filter_run focus: true
  config.formatter = :documentation
  config.order = :random
  config.run_all_when_everything_filtered = true
end
